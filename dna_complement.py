dna_sequence = open('sequence.txt').read().rstrip()

dna_complement = {'A': 'T', 'T': 'A', 'G': 'C', 'C': 'G'}
result = ""

for i in range(0, len(dna_sequence)):
    result += dna_complement[dna_sequence[i]]

print(result[::-1])

