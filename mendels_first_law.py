# Reading file for input
f = open('population.txt', 'r').read()

li = []

register = ""
for i in range(0, len(f)):
    if f[i] != " ":
        register += str(f[i]).rstrip()
    else:
        li.append(int(register))
        register = ""
    if i == (len(f)-1):
        li.append(int(register))

print(li)

# Defining variables
k = li[0] # AA
m = li[1] # Aa
n = li[2] # aa
pop = k + m + n

# Math
answer =  k / pop + (m / pop) * (k / (pop - 1))+ (m / pop) * ((m - 1) / (pop - 1)) * 0.75+ (m / pop) * (n / (pop - 1)) * 0.5 + (n / pop) * (k / (pop - 1)) + (n / pop) * (m / (pop - 1)) * 0.5
print(answer)
