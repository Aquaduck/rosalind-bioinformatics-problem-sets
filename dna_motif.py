# Open and parse source file
f = open('motif.txt', 'r')
flines = f.readlines()

# Declare/Assing variables
dna = flines[0].rstrip()
substring = flines[1].rstrip()
positions = []

# Printing values of dna and substring to ensure file was parsed correctly
print(dna)
print(substring)

# For every position, check if motif exists in string
for i in range(0, len(dna)):
    if dna[i:(i+len(substring))] == substring:
        positions.append(i+1)

# Print positions list in format that was asked for
for i in positions:
    print(i, end=" ")
