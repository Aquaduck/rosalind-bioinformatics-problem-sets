import time
import pdb
codons = {"UUU": 'F', "UUC": 'F', "UUA": 'L', "UUG": 'L', "UCU": 'S', "UCC": 'S', "UCA": 'S', "UCG": 'S', "UAU": 'Y',  "UAC": 'Y', "UAA": 'Stop', "UAG": 'Stop', "UGU": 'C', "UGC": 'C', "UGA": 'Stop', "UGG": 'W', "CUU": 'L', "CUC": 'L', "CUA": 'L', "CUG": 'L', "CCU": 'P', "CCC": 'P', "CCA": 'P', "CCG": 'P', "CAU": 'H', "CAC": 'H', "CAA": 'Q', "CAG": 'Q', "CGU": 'R', "CGC": 'R', "CGA": 'R', "CGG": 'R', "AUU": 'I', "AUC": 'I', "AUA": 'I', "AUG": 'M', "ACU": 'T', "ACC": 'T', "ACA": 'T', "ACG": 'T', "AAU": 'N', "AAC": 'N', "AAA": 'K', "AAG": 'K', "AGU": 'S', "AGC": 'S', "AGA": 'R', "AGG": 'R', "GUU": 'V', "GUC": 'V', "GUA": 'V', "GUG": 'V', "GCU": 'A', "GCC": 'A', "GCA": 'A', "GCG": 'A', "GAU": 'D', "GAC": 'D', "GAA": 'E', "GAG": 'E', "GGU": 'G', "GGC": 'G', "GGA": 'G', "GGG": 'G'}

f = open('rna_string.txt', 'r')
rna = f.readline()
protein = ""
holder = ""                                                     # Stores sequences of 3 nucleotides for checking
start_codon = 0                                                 # 0 = Start codon not found
                                                                # 1 = Start codon found
                                                                # 2 = Stop codon found
start_position = 0                                              # What position does the start codon end at?
start_modulo = start_position % 3

# Finding the start codon
while start_codon == 0:                                         # While we haven't found the start codon
    for i in range(0, len(rna)):
        holder += rna[i]                                        # Adding nucleotides to our holder variable
        print(holder)
        if holder == "AUG":
            print("Start codon found at position " + str(i-2))  # Return position of beginning of start codon
            start_codon = 1                                     # Start codon has been found
            start_position = i+1                                # Store position of first character after start codon
            break
        if len(holder) == 3:                                    # If holder is 3 nucleotides long...
            holder = holder[1:]                                 # Remove first nucleotide from holder

while start_codon == 1:
    for i in range(start_position, len(rna)):
        if len(holder) == 3:                                    # If holder is 3 chars long...
            # if ((i)%3 == start_modulo):                           # Maintains reading frame by checking if the modulo of current location is equal to modulo of the nucleotide after the last nucleotide of the start codon                                       
            if codons[holder] == 'Stop':                        # If we find a stop codon...
                start_codon = 2                                 # Set start_codon to 2 to break out of while loop
                break
            else:
                protein += codons[holder]                       # Add the corresponding AA to protein string
                print(protein)
            holder = ""                                         # Set holder to empty
        holder += rna[i]                                        # Add new char to holder
        print(holder)
print(str(protein))
