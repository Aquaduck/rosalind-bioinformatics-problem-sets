def parse_fasta(fasta):
    
    # Get list of lines from source FASTA file
    f = open(fasta, 'r')
    flist = f.readlines()

    # Declare lists for storing IDs and DNA sequences
    dna_id = []
    dna_sequences = []

    # Create list of IDs
    for i in range(0, len(flist)):
        string = flist[i]
        if string[0] == ">":
            dna_id.append(string[1:(len(string)-1)])

    # Use register to build strings
    register = ""

    # Create list of DNA sequences
    for i in range(1, len(flist)):
        string = flist[i]
        if string[0] != ">":                        # If the line is not an ID line
            register += str(flist[i]).rstrip()      # add the line to the register string
        else:
            dna_sequences.append(register)          # add the register as a new list item to dna_sequences
            register = ""                           # clear the register
        if i == (len(flist)-1):                     # If this is the last line in the file
            dna_sequences.append(register)          # add the register as a new list item to dna_sequences

    return dna_id, dna_sequences
