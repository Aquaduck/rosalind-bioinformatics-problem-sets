# Reading file and returning a list of each line
f = open('hamming_distance.txt', 'r')
flist = f.readlines()

# Assigning the lines to variables
s = flist[0]
t = flist[1]

# Initializing variable to store hamming distance
hamming_distance = 0

# Iterate through string and add 1 to hamming_distance for every difference between s and t
for i in range(0, len(s)):
    if s[i] != t[i]:
        hamming_distance += 1

# Print result to console
print(hamming_distance)
