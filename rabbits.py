# Pulling integers from file
h = open('numbers.txt', 'r')
content = h.read()
print(content)

# Setting variables from file
n = 29
k = 4

# Math
pairs = [1, 1]

for i in range(1, n-1):
    pairs.append(pairs[i] + (pairs[i-1]*k)) 
    
# Output
print(pairs)
