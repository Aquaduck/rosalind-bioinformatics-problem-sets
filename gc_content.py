import fasta_parser

# Defining functions
def get_gc_content(sequence):
    gc = len(list(filter(lambda x: (x == "G" or x == "C"), sequence)))      # Filter only G and C nucleotides
    gc_content = (gc / len(sequence)) * 100                                 # Calculate percentage of GC
    return gc_content

def compare_gc_content(dna_strings):
    gc_content = []                                                         # Creating list to store all GC content percentages
    for i in range(0, len(dna_strings)):
        gc_content.append(get_gc_content(dna_strings[i]))                   # Add each GC content to gc_content in order
    return gc_content.index(max(gc_content)), max(gc_content)               # Return a tuple of the index of the greatest gc content as well as its percentage

# Running fasta_parser.py to return FASTA IDs and DNA sequences in two lists
fasta_ids = fasta_parser.parse_fasta('gc_content.txt')[0]
fasta_sequences = fasta_parser.parse_fasta('gc_content.txt')[1]

greatest_gc_content = compare_gc_content(fasta_sequences)                   # Running compare_gc_content against fasta_sequences

# Print ID and GC percentage of sequence with greatest GC content
print(fasta_ids[greatest_gc_content[0]])
print(greatest_gc_content[1])
